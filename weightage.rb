class Parent

  attr_accessor :name, :keywords

  def initialize name, keywords
    @name = name
    @keywords = keywords
  end

end

class Page < Parent

  MAX_KEYWORDS = 8

  def calculate_weightage(query)
    # Calculate weightage of common keywords
    sum = 0
    (query.keywords & keywords).each do |keyword|
      sum += query.value(keyword) * value(keyword)
    end
    sum
  end

  private

  def value(keyword)
    # return weightage for a keyword
    MAX_KEYWORDS - keywords.index(keyword)
  end

end

class Query < Parent

  MAX_KEYWORDS = 8

  def value(keyword)
    # return weightage for a keyword
    MAX_KEYWORDS - keywords.index(keyword)
  end

end

class Weightage

  attr_reader :pages, :queries, :result

  def initialize items
    # Split input to pages and queries
    page, query = Hash.new, Hash.new
    i = j = 0
    items.downcase.split(/[ \n]?([pq])[ \n]/).tap(&:shift).each_slice(2).map {|k, v| 
      k == 'p' ? page["#{k}#{i += 1}"] = v.split(/[ \n]/) : query["#{k}#{j += 1}"] = v.split(/[ \n]/)}

    build_pages(page)
    build_queries(query)
  end

  def calculate
    # Calculate weightage of pages on each query
    @result = Hash.new
    @queries.each do |query|
      @result[query.name] = Hash.new
      @pages.each do |page|
        @result[query.name][page.name] = page.calculate_weightage(query)
      end
    end
  end

  def printout
    @result.each do |result|
      puts "#{result.first.upcase}: #{result.last.sort_by { |k, v| [-v, k] }[0..4].to_h.delete_if {|_k, v| v == 0}.keys.join(' ').upcase}"
    end
  end

  private

  def build_pages(page)
    @pages = []
    page.each do |name, keywords|
      @pages << Page.new(name, keywords)
    end
  end

  def build_queries(query)
    @queries = []
    query.each do |name, keywords|
      @queries << Query.new(name, keywords)
    end
  end

end

$/ = "\n\n"
items = gets.chomp
weightage = Weightage.new items
weightage.calculate
weightage.printout
